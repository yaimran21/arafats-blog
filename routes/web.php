<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/blogs/{slug}',["as"=>"blog.single","uses"=>"BlogController@getSingle"])->where('slug','[\w\d\-\_]+');
Route::get('/blogs',["as"=>"blog.index", "uses"=>"BlogController@getIndex"]);
Route::get('/',"pageController@getHomePage");
Route::get('/contact',"pageController@getContactPage");
Route::get('/about',"pageController@getAboutPage");
Route::resource('posts','PostController');

// Route::get('/home', 'HomeController@index')->name('home');
