<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class pageController extends Controller
{
	public function getHomePage()
	{ 
		$posts=Post::paginate(5);
		return view('pages.home')->with("posts",$posts);
	} 
	public function getContactPage()
	{
		return view('pages.contact');
	} 
	public function getAboutPage()
	{
		return view('pages.about');
	}
}
