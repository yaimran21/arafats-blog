<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Session;
class PostController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'DESC')->paginate(5);
         return view('posts.index')->with('posts',$posts);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'title' => "required|max:255",
            'Slug' => "required|max:255|alpha_dash|unique",
            'content' => "required",
             ));
        $post= new Post;
        $post->title=$request->title;
        $post->slug=$request->title;
        $post->content=$request->content;
        $post->save();

       Session::flash('success',"The blog post was successfully save!!!");
        return redirect()->route('posts.show',$post->id);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post= Post::find($post->id);
        return view('posts.show')->with('post',$post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {    
        // $post= Post::find($post->id);
        return view('posts.edit')->with('post',$post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, array(
            'title' => "required|max:255",
            'slug' => "required|max:255|alpha_dash|unique:posts,slug",
            'content' => "required",
             ));
        $post=Post::find($post->id);
        $post->title=$request->input('title');
        $post->slug=$request->input('slug');
        $post->content=$request->input('content');
        $post->update();
        Session::flash('update','The blog post was successfully updated!!!');
        return redirect()->route('posts.show',$post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post= Post::find($post->id);
        $post->delete($post->id);
      Session::flash('delete','The blog post was successfully deleted!!!');
        return redirect()->route('posts.index');
        // return redirect('posts.index');
    }
}
