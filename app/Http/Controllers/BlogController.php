<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    public function getIndex(){
		$posts= Post::paginate(5);
		return view('blogs.index')->withPosts($posts);
	}
	public function getSingle($slug){
		$post= Post::where("slug",'=',$slug)->first();
		return view('blogs.single')->with('post',$post);
	}

}
