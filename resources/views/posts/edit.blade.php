@extends('main')
@section('content')
@section('title',"Arafat's Blog||Update Posts")
@section('stylesheets')
{!!Html::style('css/parsley.css')!!}
@endsection
<div class="container"> 
    <div class="row">
        <div class="col-md-9 col-md-offset-1">
            <div class="panel panel-info">
                <div class="panel-heading text-center">Update Post</div>
                <div class="panel-body">
                <a href="{{ url('/posts') }}" title="Cancel"><button class="btn btn-success btn-xs">Cancel</button></a>
                    <br />
                    <br />
                    @include('partials._error')
                    @include('forms.editPost')
        </div>
    </div>
</div>
</div>
</div>
@endsection
@section('scripts')
{!!Html::script('js/parsley.min.js')!!}
@endsection