@extends('main')
@section('title',"Arafat's Blog||Create New Post")
@section('content')
@section('stylesheets')
{!!Html::style('css/parsley.css')!!}
@endsection
<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-offset-1">
            <div class="panel panel-info">
                <div class="panel-heading text-center">Create New Post</div>
                <div class="panel-body">
                    <a href="{{ url('/') }}" title="Back"><button class="btn btn-primary btn-md"> Back</button></a>
                    <br />
                    <br />
                    @include('partials._error')
                    @include('forms.createPost')
        </div>
    </div>
</div>
</div>
</div>
@endsection
@section('scripts')
{!!Html::script('js/parsley.min.js')!!}
@endsection