@extends('main')
@section('title',"Arafat's Blog||See All Post")
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10">
      <h1 class="text-center">All Posts</h1>
      <hr/>
      @include('partials._messages')
      @foreach($posts as $post)
      <div class="panel panel-info">
        <div class="panel-heading ">  <h4>{{ $post->title }}</h4></div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-10">
              <p>{{ substr($post->content,0,50) }} {{strlen($post->content)>50?".....":""}}</p>
              <b> Created At: {{ date('M j, Y h:i a',strtotime($post->created_at))}}</b>
            </div>
            <div class="col-md-2">
              <div class="col-md-6">
              {!! Html::linkRoute('posts.show','',array($post->id),array("class"=>" btn btn-info btn-block fa fa-eye")) !!}
              </div>
              <div class="col-md-6">
                {!! Html::linkRoute('posts.edit','',array($post->id),array("class"=>" btn btn-primary btn-block fa fa-pencil-square-o")) !!}
              </div> 
          </div>
        </div>
      </div>
    </div>
    @endforeach
    <div class="text-center" >
      {!! $posts->links();!!}
    </div>
  </div>
  <div class="col-md-2">
    <div class="well">
      <h3> {!! Html::linkRoute('posts.create','Create New Post',array(null),array("class"=>" btn btn-success")) !!}
      </h3>
      <hr/>
    </div>  
  </div>
</div>
</div>
@endsection