@extends('main')
@section('content')
@section('title',"Arafat's Blog||show post")
<div class="container">
    <div class="row">
     <div class="col-md-8">
        <div class="panel panel-info">
            <div class="panel-heading text-center ">  <h3>{{ $post->title }}</h3></div>
            @include('partials._messages')
            <p class="text-center">{{$post->content}}</p>
            </div>
        </div>
        <div class="col-md-4">
        <div class="well">
          <dl class="dl-horizontal">
              <label>Created At:</label>
              <p> {{ date('M j, Y h:i a',strtotime($post->created_at))}}</p>
        </dl>
         <dl class="dl-horizontal">
              <label>Updated At:</label>
              <p> {{ date('M j, Y h:i a',strtotime($post->updated_at))}}</p>
        </dl> 
        <dl class="dl-horizontal text-left">
              <label>Url:</lebel>
              <p> <a href="{{ url('blog',$post->slug) }}">{{ url('blog',$post->slug) }}</a></p>
        </dl> 
        <hr/>
        <div class="row">
            <div class="col-md-6">
            {!! Html::linkRoute('posts.edit','Edit',array($post->id),array("class"=>" btn btn-success btn-block")) !!}
            </div>
            <div class="col-md-6">
             {{ Form::open(['method' => 'DELETE', 'Route' => 'posts.destroy', $post->id]) }}
                {{ Form::hidden('id', $post->id) }}
                {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) }}
            {{ Form::close() }}
            </div>
            <div class="row">
              <div class="col-md-12">
                {{Html::linkRoute("posts.index",'<<See All Posts',[],["class"=>' btn-margin btn btn-primary btn-block text-center'])}}
              </div>
            </div>
        </div>
        </div> 
         </div>   
         </div>
    </div>
</div>
</div>
@endsection