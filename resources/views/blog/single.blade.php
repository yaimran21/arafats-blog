@extends('main')
@section('content')
@section('title',"Arafat's blog|| $post->slug")
<div class="container">
    <div class="row">
     <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading text-center">  <h3>{{ $post->title }}</h3></div>
            @include('partials._messages')
            <p class="text-center">{{$post->content}}</p>
            </div>
        </div> 
         </div>
    </div>
</div>
</div>
@endsection