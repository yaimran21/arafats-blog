@extends('main')
@section('title',"Arafat's-Blog||Home")
@section('content')
<div class="row">
  <div class="jumbotron">
  <h1>Welcome to Your Blog!!!</h1>      
  <p> Thank you so mouch for visiting.We try to makeing a usefull blog. Please reade your popular post</p>
  <a class="btn btn-primary btn-lg" href="#" role="button">Popular Blog</a>
</div>
<hr/>
  
<div class="row">

  <div class="col-md-8 ">
  @foreach($posts as $post)
  <div class="post">
  <h1>{{$post->title}}</h1>
    <p>{{substr($post->content,0,300)}}{{strlen($post->content)>300?"...........":''}}</p>

      <b> Created At: {{ date('M j, Y h:i a',strtotime($post->created_at))}}</b><br/>
    {!! Html::linkRoute('blog.single','Read More',array($post->slug),array("class"=>" btn btn-info btn-xs")) !!}
    </div>
    @endforeach
  </div>
    <div class="col-md-2 col-md-offset-2">
    <h3>Sidebar</h3>
    <hr/>
  </div>
</div>
  <div class="text-center" >
        {!! $posts->links();!!}
      </div>
</div>
@endsection
