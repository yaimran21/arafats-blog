@extends("main")
@section('title',"Arafat's-Blog||Contact")
@section('content')
<div class="row">
<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-info text-center">
			<div class="panel-heading">Contact With Me</div>  
			<div class="panel-body">
				@include('forms.contactMe')
			</div>
		</div>
		</div>
	</div>
@endsection