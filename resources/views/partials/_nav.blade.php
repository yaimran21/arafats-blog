 <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Arafat's-Blog</a>
    </div>
    @if(Auth::user())
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="{{ Request::is('/')?'active':'' }}"><a href={{ url('/') }}><i class="fa fa-home" aria-hidden="true"> Home </i></a></li>
        <li class="{{ Request::is('contact')?'active':'' }}"><a href={{ url('/contact') }}><i class="fa fa-phone" aria-hidden="true"></i> Contact</a></li>
        <li class="{{ Request::is('about')?'active':'' }}"><a href={{ url('/about') }}> <i class="fa fa-address-book" aria-hidden="true"></i> About</a></li>
        <li class=" {{ Request::is('blogs')?'active':'' }}"><a href={{ url('/blogs')}}> <i class="fa fa-book" aria-hidden="true"></i> Blog</a></li>
      </ul>
    {{--   <ul class="nav navbar-nav navbar-left">
         </ul> --}}

         <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> {{Auth::user()->name}} <span class="caret"></span></a>
            <ul class="dropdown-menu">
                  <li class="dropdown">
            <li class="{{ Request::is('posts')?'active':'' }}"><a href={{ url('/posts') }}> <i class="fa fa-tasks" aria-hidden="true"></i> All-Post</a></li>
               <li class="{{ Request::is('posts/create')?'active':'' }}"><a href={{ url('/posts/create') }}><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Create-Post</a>
             </li>
              <li>
                <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out" aria-hidden="true"></i>
                Logout
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
            </li>
        </ul>
      </li>
    </ul>
  </div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
@else
<ul class="nav navbar-nav">
  <li class="{{ Request::is('/')?'active':'' }}"><a href={{ url('/') }}><i class="fa fa-home" aria-hidden="true"> Home </i></a></li>
  <li class="{{ Request::is('contact')?'active':'' }}"><a href={{ url('/contact') }}><i class="fa fa-phone" aria-hidden="true"></i> Contact</a></li>
  <li class="{{ Request::is('about')?'active':'' }}"><a href={{ url('/about') }}> <i class="fa fa-address-book" aria-hidden="true"></i> About</a></li>
  <li class=" {{ Request::is('blogs')?'active':'' }}"><a href={{ url('/blogs')}}> <i class="fa fa-book" aria-hidden="true"></i> Blog</a></li>
</ul>
<ul class="nav navbar-nav navbar-right">
  <!-- Trigger the modal with a button -->
  <li><a href="#" data-toggle="modal" data-target="#loginModal"> <i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li>
  <li><a href="#" data-toggle="modal" data-target="#registerModal"><i class="fa fa-arrow-up" aria-hidden="true"></i> Register</a></li>
</ul>
{{-- Modal-Start --}}
@include('modals.login-modal')
@include('modals.register-modal')
{{-- Modal-End --}}
@endif
</nav>