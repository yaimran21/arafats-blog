
@if(Session::has('success'))
<div class="alert alert-success text-center" role="alert">
	<strong>Success:</strong> {{Session::get('success')}}
</div>
@elseif(Session::has('update'))
<div class="alert alert-success text-center" role="alert">
	<strong>Success:</strong> {{Session::get('update')}}
</div>
@elseif(Session::has('delete'))
<div class="alert alert-danger text-center" role="alert">
	<strong>Success:</strong> {{Session::get('delete')}}
</div>
@endif