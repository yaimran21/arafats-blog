{!! Form::open(['url' => 'posts', 'class' => 'form-horizontal', 'files' => true,'data-parsley-validate'=>'']) !!}
{{ method_field('post') }}
{{ csrf_field() }}
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
 {!! Form::label('title', 'Post Title:', ['class' => 'col-md-2  control-label']) !!}
 <div class="col-md-10">
  {!! Form::text('title', null, ['class' => 'form-control','placeholder'=>'Please Input Title......','required'=>'','data-parsley-length'=>"[6, 255]"]) !!}
  {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
</div>
<div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}">
 {!! Form::label('title', 'Post Slug:', ['class' => 'col-md-2  control-label']) !!}
 <div class="col-md-10">
  {!! Form::text('slug', null, ['class' => 'form-control','placeholder'=>'Please Input Slug......','required'=>'','data-parsley-length'=>"[6, 255]"]) !!}
  {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
</div>
</div>

<div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
  {!! Form::label('content', 'Post Content:', ['class' => 'col-md-2 control-label']) !!}
  <div class="col-md-10">
    {!! Form::textarea('content', null, ['class' => 'form-control','placeholder'=>'Please Input Content......','required'=>'','data-parsley-length'=>"[10, 2000]"]) !!}
    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="form-group">
 <div class="col-md-offset-2 col-md-10">
   {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create Post', ['class' => 'btn btn-success btn-lg btn btn-block']) !!}
 </div>
</div>
{!! Form::close() !!}
