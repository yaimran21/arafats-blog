{!! Form::open(['url' => 'foo/bar','class' => 'form-horizontal', 'files' => true])!!}
				{{ method_field('post') }}
		        {{ csrf_field() }}
				<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
					{!! Form::label('email','Your Email:',['class' => 'col-md-2 control-label']	)!!}
					<div class="col-md-10">
						{!! Form::email('email',null,['class'=>'form-control','placeholder'=>'Enter Your Email']) !!} 
						{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
					</div>
				</div>
				<div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
					{!! Form::label('subject','Subject:',['class' => 'col-md-2 control-label'])!!}
					<div class="col-md-10">
						{!! Form::text('subject',null,['class'=>'form-control','placeholder'=>'Enter Subject']) !!}
						{!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
					</div>
				</div>
				<div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
					{!! Form::label('message','Message:',['class' => 'col-md-2 control-label'])!!}
					<div class="col-md-10">
						{!! Form::textarea('message',null,['class'=>'form-control','placeholder'=>'Leave Your Message...........']) !!}
					</div>
				</div>
				<div class="form-group ">
					<div class="col-md-10 col-md-offset-2">
                     {!! Form::submit('submit',['class'=>'btn btn-success btn-lg btn-block']) !!}					
                     </div>
				</div>
				
				{!! Form::close() !!}